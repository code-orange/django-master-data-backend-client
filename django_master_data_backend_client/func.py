from base64 import b64decode

import requests
import os


def master_data_backend_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "MASTER_DATA_BACKEND_API_URL",
            default="https://master-data-backend-api.example.com/",
        )

    return settings.MASTER_DATA_BACKEND_API_URL


def master_data_backend_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("MASTER_DATA_BACKEND_API_USER", default="user")
        password = os.getenv("MASTER_DATA_BACKEND_API_PASSWD", default="secret")
        return username, password

    username = settings.MASTER_DATA_BACKEND_API_USER
    password = settings.MASTER_DATA_BACKEND_API_PASSWD

    return username, password


def master_data_backend_head(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.head(
        f"{master_data_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def master_data_backend_get_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.get(
        f"{master_data_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def master_data_backend_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{master_data_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def master_data_backend_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{master_data_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def master_data_backend_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{master_data_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def master_data_backend_delete_data(
    endpoint: str, api_user: str, api_password: str, params=None
):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{master_data_backend_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def master_data_backend_get_api_credentials_for_customer(mdat_id: int):
    username, password = master_data_backend_default_credentials()

    if username != mdat_id:
        response = master_data_backend_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def master_data_backend_get_customers(mdat_id: int):
    username, password = master_data_backend_get_api_credentials_for_customer(mdat_id)

    response = master_data_backend_get_data(
        "v1/mdatcustomers",
        username,
        password,
    )

    if "objects" not in response:
        return list()

    return response["objects"]


def master_data_backend_get_customer(mdat_id: int, customer_id: int):
    username, password = master_data_backend_get_api_credentials_for_customer(mdat_id)

    response = master_data_backend_get_data(
        f"v1/mdatcustomers/{customer_id}",
        username,
        password,
    )

    return response


def master_data_backend_create_customer(mdat_id: int, customer_data):
    username, password = master_data_backend_get_api_credentials_for_customer(mdat_id)

    response = master_data_backend_post_data(
        "v1/mdatcustomers",
        username,
        password,
        data=customer_data,
    )

    return response


def master_data_backend_get_item_datasheet(mdat_id: int, item_id: int):
    username, password = master_data_backend_get_api_credentials_for_customer(mdat_id)

    response = master_data_backend_get_data(
        f"v1/mdatitemsdatasheet/{item_id}",
        username,
        password,
    )

    return b64decode(response["pdf"])
